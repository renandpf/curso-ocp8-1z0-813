Estudos práticos para certificação OCJP 8 (upgrade). 
Conteúdo: 
01 - Language Enhancements 
02 - Concurrency 
03 - Localization 
03 - Date/Time API 
04 - NIO 2 
05 - Lambda Expressions 
06 - Java Collections 
07 - Stream API