package br.com.pupposoft.ocp8.concurrency;

public class WorkingWithParallelStreams {

	
	/**
	 * Para o exame é preciso saber duas formas de criar stream em paralelo.
	 * 	parallel()
	 * 	- Usado para criar um stream paralela de um stream existente.
	 * 	- ex:
	 * 		Stream<Integer> stream = Arrays.asList(1,2,3,4,5,6).stream();
	 * 		Stream<Integer> parallelStream = stream.parallel();
	 * 
	 * parallelStream()
	 * 	- Usado para criar de uma coleção	
	 * 	- ex:
	 * 		Stream<Integer> parallelStream2 = Arrays.asList(1,2,3,4,5,6).parallelStream();
	 * 
	 * A interface Stream possui um método "isParallel()" que permite identificar se o objeto é ou não 
	 * paralelo. 
	 * 
	 * Atentar que algumas operações mantem o stream paralelo, já outros não. 
	 * Por exemplo, o método "Stream.concat(Stream s1, Stream s2)" é paralelo se s1 ou s2 for paralelo.
	 * Já o método "flatMap()" cria um novo stream não paralelo. 
	 * 
	 */
	public void creatingParallelStreams() {
		
	}
	
	
	/**
	 * 
	 * Utilizar parallelStream é equivalente a submeter multiplos "Runnable" para um pool de threads.
	 * 	- O código abaixo imprime a coleção em qualquer ordem (diferente se fosse usado "stream" ao invés de "parallelStream".
	 * 	- Arrays.asList(1,2,3,4,5,6).parallelStream().forEach(s -> System.out.print(s+" "));
	 * 
	 * O seguinte código abaixo, irá imprimir os valores em série.
	 * 	- Arrays.asList(1,2,3,4,5,6).parallelStream().forEachOrdered(s -> System.out.print(s+" "));
	 * 
	 * Não  existe uma garantia de que o uso de um fluxo paralelo melhorará o desempenho. De fato,
	 * O uso de um fluxo paralelo pode atrasar a aplicação devido à sobrecarga de criar estruturas de processamento paralelas.
	 * Uma série de fatores devem ser analisadas antes de usar stream paralelo.
	 * 
	 * 
	 * Para o uso de stream paralelo, as operações devem ser indepedentes.
	 *	- No código abaixo, o mapping de "jackal" para "JACKAL" não interfere no "kangaroo" para "KANGAROO". Desta forma o resultado não é afetado.   
	 * 	Arrays.asList("jackal","kangaroo","lemur")
	 *		.parallelStream()
	 *		.map(s -> s.toUpperCase())
	 *		.forEach(System.out::println);
	 *	
	 * - Já no código abaixo, o resultado pode ser diferente. Mesmo que o retorno das operações do map sejam a mesma, o momento de processamento
	 * 	 pode não ser. 
	 * Arrays.asList("jackal","kangaroo","lemur")
	 * 		.parallelStream()
	 * 		.map(s -> {System.out.println(s); return s.toUpperCase();})
	 *		.forEach(System.out::println);
	 * Possível resultado:
	 * 		kangaroo, 
	 * 		KANGAROO, 
	 * 		lemur, 
	 * 		jackal, 
	 * 		JACKAL, 
	 * 		LEMUR
	 * 
	 * Operações Stateful devem ser evitadas quando se usa operações com stream paralelas.
	 * - Considere o seguinte trecho de código abaixo:
	 * 	List<Integer> data = Collections.synchronizedList(new ArrayList<>());
	 * 	Arrays.asList(1,2,3,4,5,6).parallelStream()
	 * 		.map(i -> {data.add(i); return i;}) // AVOID STATEFUL LAMBDA EXPRESSIONS!
	 *		.forEachOrdered(i -> System.out.print(i+" "));
	 * 	System.out.println();
	 * 	for(Integer e: data) {
	 *		System.out.print(e+" ");
	 *	}
	 *
	 *	Retorno possível 
	 * 		1 2 3 4 5 6
	 *		2 4 3 5 6 1
	 * 
	 * 	Porém, se fosse usar stream não paralela, o resultado seria:
	 * 		1 2 3 4 5 6
	 *		1 2 3 4 5 6 
	 * 
	 * 
	 */
	public void processingTasksInParallel() {
		
	}
	
	/**
	 * Como a ordem não é garantida em fluxos paralelos, métodos como "findAny()" em fluxos paralelos pode ter um resultado inesperado.
	 * Veja os exemplos:
	 * 
	 * O código abaixo, sempre irá retornar "1". Se fosse utilizado "parallelStream()" o retorno poderia ser qualquer um da coleção.
	 * System.out.print(Arrays.asList(1,2,3,4,5,6).stream().findAny().get());
	 * 
	 * Qualquer operação baseado em ordem, como "findFirst()", "limit()" ou "skip()" vai onerar a performance. 
	 * Isso ocorre pelo fato do processamento em paralelo ter que coordenar a sincronização.
	 * 
	 * Por outro lado, o resultado de operações de ordem são sempre iguais para fluxos serias e paralelos (TODO: testar). 
	 * 
	 * TODO: Parei em "Combining Results with reduce()", pag 373
	 * 
	 * 
	 */
	public void processingParallelReductions() {
		
	}
	
	
}
