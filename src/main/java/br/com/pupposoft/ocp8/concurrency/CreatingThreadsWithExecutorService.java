package br.com.pupposoft.ocp8.concurrency;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CreatingThreadsWithExecutorService {
//http://www.baeldung.com/java-executor-service-tutorial
	
	public void instanciando() {
		
		//Factory Methods of the Executors Class
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		
		//Directly Create an ExecutorService
		executorService = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS,new LinkedBlockingQueue<Runnable>());
		
		executorService.shutdown();
	}
	
	
	/**
	 * void execute(Runnable command): 
	 * executa uma runnable task em algum momento do futuro.
	 * 
	 * Future<?> submit(Runnable task): 
	 * executa uma runable task em algum momento do futuro e retorna um Future representando a task
	 * 
	 * <T> Future<T> submit(Callable<T> task): 
	 * executa uma callable task em algum momento do futuro e retorna um Future representando o resultado pendente da task
	 * 
	 * <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException:
	 * executa uma lista de task callable sincrono e retorna uma coleção de objetos Future na mesma ordem da coleção das tasks
	 * - Notar que o retorno só ocorre após execução de todas as tasks.
	 * - O método Future.isDone() irá retornar "true" indepente se a task terminou com sucesso ou com exception.
	 * - Existe outro método (sobrecarga) com um parametro de timeout (TimeUnit) 
	 * 
	 * <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException
	 * executa as tasks recebidas sincrono e retorna o resultado de uma que foi sucesso, cancelando em todas as tasks não terminadas.
	 * - Não é garantido qual será retornada (pode ser qualquer uma, independente da ordem).
	 * - Existe outro método (sobrecarga) com um parametro de timeout (TimeUnit)
	 * 
	 */
	@SuppressWarnings("unused")
	public void adicionandoTasksParaExecutorService() {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		
		Runnable runnableTask = () -> System.out.println("A");
		List<Runnable> runnableTasks = Arrays.asList(runnableTask, runnableTask);
		
		Callable<String> callableTask = () -> "teste";
		List<Callable<String>> callableTasks = Arrays.asList(callableTask, callableTask);
		
		
		//"execute" - Aceita somente "Runnable". Não possui retorno (void) pra saber o status da execução.
		executorService.execute(runnableTask);
		//executorService.execute(callableTask); //Erro de compilação
		
		//"submit" - Aceita runnable e callable. Possui retorno para verificar o status
		Future<?> result =  null;
		result = executorService.submit(runnableTask);
		result = executorService.submit(callableTask);
		
		//"invokeAny" - Aceita somente callable. Executa cada uma até uma ter sucesso. O retorno da execução é obtido 
		try {
			String result2 = executorService.invokeAny(callableTasks);
			//executorService.invokeAny(runnableTasks); //Erro de compilação
		} catch (InterruptedException | ExecutionException e) {}
		
		//"invokeAll" - Aceita somente callble. Executa todas as tarefas e retora seus resultados
		try {
			List<Future<String>> results = executorService.invokeAll(callableTasks);
			//results = executorService.invokeAll(runnableTasks); //Erro
			
		} catch (InterruptedException e) {}
	}
	
	/**
	 * executorService.shutdown()
	 * 	- Espera todas as threads terminar, para depois encerrar o poll
	 * 	- Não aceita mais tasks
 	 *	
 	 *	executorService.shutdownNow()
 	 *	 - Não aceita mais tasks
 	 *	 - Tenta encerrar todas as tasks em execução e descarta as que ainda não iniciaram
	
	 *
	 *	Para os 2 casos:
	 *	 - executorService.isShutdown() = true
	 *	 - executorService.isTerminated() = false (se ainda houver tasks não terminadas)
	 * 
	 */
	@SuppressWarnings("unused")
	public void executorServiceShutDown() {
		ExecutorService executorService = null;
		try {
			executorService = Executors.newSingleThreadExecutor();
			
			//adicionas tasks, ...
			
		} finally {//Não é AutoCloseable, portanto não pode usar "try-with-resource"
			if(executorService != null) {
				//Espera cada tarefa terminar, para depois encerrar o poll
				executorService.shutdown();
			}
		}
		
		//********
		
		try {
			executorService = Executors.newSingleThreadExecutor();
			
			//adicionas tasks, ...
			
		} finally {
			if(executorService != null) {
				//TENTA terminar todas as threads em execução e descarta as que ainda não comecaram.
				//PS: é possível criar threads que nunca terminam. Então, qualquer tentativa de termina-las serão ignoradas.
				List<Runnable> listaTarefasNuncaForamExecutadas = executorService.shutdownNow();
			}
		}
		
	}
	
	public void cicloVidaExecutorService() {
		ExecutorService executorService = null;
		try {
			executorService = Executors.newSingleThreadExecutor();
			executorService.execute(() -> System.out.println("AAA"));

			//No status de "active" o executorService aceita novas tasks e executa cada uma delas.
			System.out.println("executorService.isShutdown: "+executorService.isShutdown());//false
			System.out.println("executorService.isTerminated: "+executorService.isTerminated());//false
			
			
		} finally {
			if(executorService != null) {
				executorService.shutdown();
				
				//No status de "shuttingDown" o executorService não aceita novas tasks e mas finaliza as em execução.
				System.out.println("executorService.isShutdown: "+executorService.isShutdown());//true
				System.out.println("executorService.isTerminated: "+executorService.isTerminated());//false


				//No status de "shutDow" o executorService não aceita novas tasks e contém todas as tarefas terminadas.
				System.out.println("executorService.isShutdown: "+executorService.isShutdown());//true
				System.out.println("executorService.isTerminated: "+executorService.isTerminated());//false

				
			}

		}
	}
	
	/**
	 * Os resultados são enviados pela classe Future.
	 * Future possui os seguintes métodos:
	 * 
	 *  boolean isdone()
	 *  Retorna "true" se uma task foi completada, ou lançou exception, ou foi cancelada.
	 *  
	 *  boolean isCancelled()
	 *  Retorna "true" se a task foi cancelada toltamente sem exception
	 *  
	 *  boolean cancel()
	 *  Tenta cancelar a execução de uma task
	 *  
	 *   V get()
	 *   Recebe o resultado de uma task. Fica aguardando infinitamente até ficar disponível
	 *   
	 *   V get(long timeout, TimeUnit unit)
	 *   Recebe o resultado de uma task. Se o tempo "timeout" ocorrer antes da entrega, uma exceção (checked) TimeoutException é lançada.
	 *
	 *   Detalhe que se a task for um Runnable, o valor de get (V) sempre será null, pois o retorno do método "run" é void.
	 * 
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public void aguardandoResultados() throws InterruptedException, ExecutionException {
		
		ExecutorService service = null;
		try {
			service = Executors.newSingleThreadExecutor();
			
			Future<?> result = service.submit(() -> {
				System.out.println("AAA");
			});
			
			//result.get(); //Trava até que ocorra um resultado
			result.get(1000, TimeUnit.MILLISECONDS); //Trava até ocorrer o resultado ou o timeout
			
			System.out.println("Finalizada!");
			
		} catch (TimeoutException e) {
			System.out.println("Ainda não finalizada...");
			
		}finally {
			
		}
	}
	
	/**
	 * TimeUnit.NANOSECONDS
	 * TimeUnit.MICROSECONDS
	 * TimeUnit.MILLISECONDS
	 * TimeUnit.SECONDS
	 * TimeUnit.MINUTES
	 * TimeUnit.HOURS
	 * TimeUnit.DAYS
	 */
	public void timeUnit() {}
	
	
	/**
	 * 	Callable é uma interface (funcional) similar ao Runnable. A diferença é que o método "call" disponível,
	 * 	possui retorno e pode lançar uma exceção checada.
	 *  Pode ser usado no método "submit" do "ExecutorService"
	 *  Então, deve atentar ao caso abaixo:
	 *  
	 *  
	 *  
	 *  Atentar com a extrema semelhança dessa interface (Callable) com a Supplier:
	 *  	As duas possuem métodos sem argumentos;
	 *  	As duas possuem retorno genérico
	 *  A única diferença é que a Callable pode uma excelçao genérica.
	 *  Exemplo de pegadinha: Considere os métdos abaixo numa classe:
	 *  	public static void useCallable(Callable<Integer> expression) {}
	 *  	public static void useSupplier(Supplier<Integer> expression) {}
	 *  	public static void use(Supplier<Integer> expression) {}
	 *  	public static void use(Callable<Integer> expression) {}
	 *  	public static void main(String[] args) {
	 *			useCallable(() -> {throw new IOException();}); // COMPILES
	 *			useSupplier(() -> {throw new IOException();}); // DOES NOT COMPILE
	 *			use(() -> {throw new IOException();});// DOES NOT COMPILE
	 *			use((Callable<Integer>)() -> {throw new IOException("");}); // COMPILES
	 *		}
	 * 
	 */
	@SuppressWarnings("unused")
	public void introducaoCallable() {
		Callable<String> returnTest = () -> "TESTE";
		Callable<String> exceptionTest = () -> {throw new IOException();};

		//Runnable runnable = () -> {Thread.sleep(1000);};//Não compila, devido a exception que é lançada e runnable não lança exception.
		Callable<String> callable = () -> {Thread.sleep(1000); return null;};//Compila, pois callable lança exception 
	}
	
	
	/**
	 * O método "awaitTermination" de ExecutorService faz com que seja aguardada a execução de todas as tasks adicionados no
	 * executorService. Neste método é passado o valor do timeout por parâmetro. 
	 * O método encerra (retorno liberado) depois que ocorrer um dos dois (tasks encerradas ou timeout).
	 * Se ocorrer timeout primeiro, é considerado que as tasks ainda não terminaram, ou seja: executorService.isTerminated() = false.
	 * 
	 * Notar que: 
	 * - uma task pode ser considerada encerrada quando ela termina com ou sem "Exception".
	 * - Tasks monitoradas: tanto Callable ou Runnable
	 * 
	 * @throws InterruptedException 
	 */
	public void aguardandoFimTodasTasks() throws InterruptedException {
		ExecutorService executorService = null;

		try {
			executorService = Executors.newSingleThreadExecutor();

			//Adicionas tasks...
			executorService.submit(() -> {
			
				System.out.println("task: Executando...");
				
				throw new IOException();
				
				//return "Executado";
			});


		}  finally {
			if(executorService != null) {
				executorService.shutdown();
			}
		}
		
		if(executorService != null) {
			System.out.println("Aguardando execução...");
			executorService.awaitTermination(95, TimeUnit.SECONDS);
			
			if(executorService.isTerminated()) {
				System.out.println("Execução terminada!");
			}else {
				System.out.println("Ainda em execução...");
			}
		}
	}
	
	/**
	 * "ScheduledExecutorService" é uma interface que extend a "ExecutorService".
	 * Possui métodos "schedule" que permitem schedular (agendar) tasks para serem executadas no futuro.
	 * Há sobrecarga desses métodos que permitem executar Callable ou Runnable.
	 * 
	 * 	 * Métodos de ScheduledExecutorService:
	 * - schedule(Callable<V> callable, long delay, TimeUnit unit)
	 * - schedule(Runnable command, long delay, TimeUnit unit)
	 * - scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit)
	 * 		- Cria uma nova task sempre após o período informado e independente do termino da task anterior.
	 * 		- O ponto de atenção também é se for usado um single-thread, pois se o tempo de execução da task for superior ao periodo,
	 * 		  tais terefas nunca serão terminadas.
	 * - scheduleAtFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit)
	 * 		Cria uma nova task sempre após o delay informado e se a task anterior tiver terminado o trabalho.

	 * 
	 * Todos eles retornam um "ScheduledFuture<V>" (interface que extend Future<V> e Delayed). Sendo assim, ScheduledFuture<V> é identico a 
	 * Future<V>, porém essa contém o método "long getDelay()"  
	 * 
	 * 
	 */
	@SuppressWarnings("unused")
	public void schedulingTask() {
		ScheduledExecutorService scheduleexecutorService = Executors.newSingleThreadScheduledExecutor();
		
		Callable<String> callableTask = () -> "Teste";
		Runnable runnableTask = () -> System.out.println("");
		
		ScheduledFuture<?> future_01 = scheduleexecutorService.schedule(callableTask, 100, TimeUnit.DAYS);//Callable
		ScheduledFuture<?> future_02 = scheduleexecutorService.schedule(runnableTask, 100, TimeUnit.DAYS);//Runnable
		ScheduledFuture<?> future_03 = scheduleexecutorService.scheduleAtFixedRate(runnableTask, 10, 10, TimeUnit.MINUTES);
		ScheduledFuture<?> future_04 = scheduleexecutorService.scheduleWithFixedDelay(runnableTask, 10, 10, TimeUnit.MINUTES);
	}
	
	/**
	 * Executors, possui os seguintes métodos:
	 * 	newSingleThreadExecutor()
	 * 	- Retorna um "ExecutorService"
	 * 	- Executa tarefas sequencialmente na ordem em que elas chegam.
	 * 	
	 * 	newSingleThreadScheduledExecutor()
	 * 	- Retorna "ScheduledExecutorService"
	 * 	- Equivalente ao "newSingleThreadExecutor", porém permitindo schedular ou colocar delay nas tasks
	 * 	- Executa as taks sequencialmete.
	 * 
	 * 	newCachedThreadPool()
	 * 	- Retorna um "ExecutorService"
	 * 	- Cria um pool de threads de tamanho ilimitado que cria as threads conforme for necessário.
	 * 	- Pode reusar threads já construidas
	 * 	- Deve ser usado para tarefas simples
	 * 
	 * 	newFixedThreadPool(int nThreads)
	 * 	- Retorna um ExecutorService
	 * 	- Cria um pool de threads que reutiliza um numero fixo de threads para operar na lista de threads.
	 * 	- Se a quantidade de tarefas forem maior que a quantidade de threds, as tarefas ficaram aguardando, 
	 * 	  de forma semelhante ao singleThread. 
	 * 	  De maneira que se chamar o método "newFixedThreadPool(1)", 
	 * 	  o comportamento será o mesmo que "newSingleThreadExecutor"
	 * 
	 *	newScheduledThreadPool(int nThreads)
	 *	- Retorna um ScheduledExecutorService
	 *	- Equivalente ao "newFixedThreadPool", porém permitindo schedular ou delay a execução. 
	 * 		
	 * 
	 */
	public void increasingConcurrencyWithPools() {
		
	}
	
	
	public static void main(String[] args) throws InterruptedException {
		new CreatingThreadsWithExecutorService().aguardandoFimTodasTasks();
	}
	
	
}
