package br.com.pupposoft.ocp8.concurrency;

public class UsingConcurrentCollections {

	
	
	/**
	 * Classes atômicas são classes que permitem que seus valores sejam atualizados de forma atômicas.
	 * São elas:
	 * - AtomicBoolean
	 * - AtomicInteger
	 * - AtomicIntegerArray
	 * - AtomicLong
	 * - AtomicLongArray
	 * - AtomicReference
	 * - AtomicReferenceArray
	 * 
	 * Cada classe possui métodos que são usados para manipular, incrementar/decrementar (somente para numericos,
	 * etc os primitivos.
	 * - get()
	 * - set()
	 * - getAndSet()
	 * - incrementAndGet(): equivalente ao "++value"
	 * - getAndIncrement(): equivalente ao "value++"
	 * - decrementAndGet(): equivalente ao "--value"
	 * - getAndDecrement(): equivalente ao "value--"
	 *  
	 * 
	 */
	public void atomicClasses() {
		
	}
	
	/**
	 * É possivel determinar que um trecho do código seja acessado por uma thread por vez.
	 * Para isso utiliza-se o "synchronized(monitor)", onde o monitor pode ser qualquer objeto.
	 * O lock é garantido pelo monitor, ou seja, é importante que o monitor seja sempre a mesma instância do objeto, 
	 * caso contrario o lock é perdido (para aquela instância, ja que o lock funciona por instância).
	 * Exemplo dentro do método.
	 * 
	 * Para os casos de métodos staticos os monitores devem ser a classe, como por exemplo: "SynchronizingDataAccessTest.class" 
	 * 
	 */
	public void improvingAccessWithSynchronizedBlocks() {
		
		//*** Sincronizando trecho de código.
		UsingConcurrentCollections monitor = this;
		System.out.println("Trecho não sincronizado: threads acessam ao mesmo tempo");
		synchronized(monitor) {
			//monitor = new SynchronizingDataAccessTest(); //Neste caso este código deixa de ser sincronizado, ja que a instância do monitor fica mudando...
			System.out.println("Trecho sincronizado: apenas uma thread acessa por vez. Desde que o monitor (this) seja o mesmo.");
		}
		
		//Deve ser utilizado em métodos estaticos. Se usado no método de instância, também funciona 
		synchronized(UsingConcurrentCollections.class) {
			
		}
	}
	
	
	
	/**
	 * 	Se todo o método deve ser sincronizado, java permite faze-lo de uma forma mais elegante do que
	 * 	utilizando monitor. Basta declarar o método como "synchronized". Assim todo seu conteúdo será sincronizado.
	 * 
	 *  Desta forma, as duas maneiras abaixo funcionam exatamente igual.
	 *  
	 *  private void incrementAndReport() {
	 *		synchronized(this) {
	 *			System.out.print((++sheepCount)+" ");
	 *		}
	 *	}
	 *	
	 *	private synchronized void incrementAndReport() {
	 *		System.out.print((++sheepCount)+" ");
	 *	}
	 * 
	 *  private static void incrementAndReport() {
	 *		synchronized(ShipManager.class) {//Qualquer classe
	 *			System.out.print("Finished work");
	 *		}
	 *	}
	 *	
	 *	private static synchronized void incrementAndReport() {
	 *		System.out.print("Finished work");
	 *	} 
	 * 
	 * 
	 */
	public synchronized void synchroninzingMethods() {

	}	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
}
